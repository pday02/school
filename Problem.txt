Details � School�s Out!
On the last day of school, the halls of Birkbeck Secondary were abuzz with talk of the
upcoming summer holidays. In the staff room, Mr. Knight and four of his colleagues (each
of whom teaches a different subject) couldn�t stop talking about their holiday plans. Each
teacher is going on a trip to a different state to engage in a different relaxing activity. When
the final bell of the day rang to signal the end of classes, the five teachers cheered even louder
than their students!
From the information provided, determine the subject taught by each teacher, the county
he or she is going to visit, and the activity he or she has planned.
1. Ms. Gross teaches either maths or science. If Ms. Gross is going to a nudist colony,
then she is going to Suffolk; otherwise she is going to Cornwall.
2. The science teacher (who is going body-boarding) is going to travel to Cornwall or
Norfolk.
3. Mr. McEvoy (who is the history teacher) is going to Yorkshire or Cumbria.
4. If the woman who is going to Hertfordshire is the English teacher, then she is Ms.
Appleton; otherwise, she is Ms. Parnell (who is going swimming).
5. The person who is going to Yorkshire (who isn�t the PE teacher) isn�t the one who is
going sightseeing.
6. Ms. Gross isn�t the woman who going camping.
7. One woman is going to a nudist colony on her holiday