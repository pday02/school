
%Currently has 2 solutions, and by my interpretation of the conditions both seem that all conditions are met so both can be true
%
%By running solve two solutions are found:
%
% Solution 1:
%	mrKnight who teaches science is going body_boarding in cornwall
%	msGross who teaches maths is going nudistColony in suffolk
%	mrMcEvoy who teaches history is going sightseeing in cumbria
%	msParnell who teaches pe is going swimming in hertfordshire
%	msAppleton who teaches english is going camping in yorkshire
%true ;

% Solution 2:
%	mrKnight who teaches science is going body_boarding in norfolk
%	msGross who teaches maths is going nudistColony in suffolk
%	mrMcEvoy who teaches history is going sightseeing in cumbria
%	msParnell who teaches pe is going swimming in hertfordshire
%	msAppleton who teaches english is going camping in yorkshire
%true ;

solve :- 
	%Set all subjects, activities and locations as differnt
		subject(KSub), subject(GSub), subject(MSub), subject(PSub), subject(ASub),
		alldifferent([KSub,GSub,MSub,PSub,ASub]),

		activity(KAct),activity(GAct),activity(MAct),activity(PAct),activity(AAct),
		alldifferent([KAct,GAct,MAct,PAct,AAct]),

		location(KLoc),	location(GLoc),	location(MLoc),	location(PLoc),	location(ALoc),
		alldifferent([KLoc,GLoc,MLoc,PLoc,ALoc]),

	%Set list of solution
		Solution = [[mrKnight,KSub,KAct,KLoc],[msGross,GSub,GAct,GLoc],[mrMcEvoy,MSub,MAct,MLoc],
			[msParnell,PSub,PAct,PLoc],[msAppleton,ASub,AAct,ALoc]],

	%1. Ms. Gross teaches either maths or science.
		(member([msGross,maths,_,_],Solution) ; member([msGross,science,_,_],Solution)),
	
	%If Ms. Gross is going to a nudist colony, then she is going to Suffolk; otherwise she is going to Cornwall.
		( member([msGross,_,nudistColony,suffolk],Solution) ; ( GAct \= nudistColony , member([msGross,_,_,cornwall],Solution) ) ),

	%2. The science teacher (who is going body-boarding) is going to travel to Cornwall or Norfolk.
		( member([_,science,body_boarding,cornwall],Solution) ; member([_,science,body_boarding,norfolk],Solution) ),

	%3. Mr. McEvoy (who is the history teacher) is going to Yorkshire or Cumbria.
		( member([mrMcEvoy,history,_,yorkshire],Solution) ; member([mrMcEvoy,history,_,cumbria],Solution) ),

	%4. If the woman who is going to Hertfordshire is the English teacher, then she is Ms.
	%Appleton; otherwise, she is Ms. Parnell (who is going swimming).
		( member([msAppleton,english,_,hertfordshire],Solution) ; member([msParnell,_,_,hertfordshire],Solution) ),
		member([msParnell,_,swimming,_],Solution),
	
	%5. The person who is going to Yorkshire (who isn�t the PE teacher) isn�t the one who is going sightseeing.
		%Assume this means someone must be going to yorkshire
		member([_,_,_,yorkshire],Solution),
		\+ member([_,_,sightseeing,yorkshire],Solution),
		\+ member([_,pe,_,yorkshire],Solution),
	
	%6. Ms. Gross isn�t the woman who going camping.
		member([Camper,_,camping,_],Solution),
		female(Camper),
		Camper \= msGross,

	%7. One woman is going to a nudist colony on her holiday
		member([Exhibitionist,_,nudistColony,_],Solution),
		female(Exhibitionist),

	printSolution(Solution).		



%5 teachers
teacher(mrKnight).
teacher(msGross).
teacher(mrMcEvoy).
teacher(msParnell).
teacher(msAppleton).

%5 subjects
subject(maths).
subject(science).
subject(history).
subject(pe).
subject(english).

%5 activities
activity(nudistColony).
activity(body_boarding).
activity(swimming).
activity(sightseeing).
activity(camping).

%6 locations
location(suffolk).
location(cornwall).
location(norfolk).
location(yorkshire).
location(cumbria).
location(hertfordshire).

%gender mentioned and so defined below
female(msGross).
female(msParnell).
female(msAppleton).

%all need to be different - taken "all different" method from example exercise
alldifferent([H|T]) :- member(H,T),!,fail.
alldifferent([_|T]) :- alldifferent(T).
alldifferent([]).

printSolution([H|T]) :- printLine(H) , printSolution(T).
printSolution([]).
printLine([Name,Sub,Act,Loc]) :- write(Name), write(' who teaches '), write(Sub), write(' is going '), write(Act), write(' in '), write(Loc), nl.